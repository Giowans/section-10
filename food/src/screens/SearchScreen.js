import React, {useState} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import SearchBar from '../components/SearchBar';
import useResults from '../hooks/useResults';
import ResultList from '../components/ResultList';

const SearchScreen = () =>
{
    const [text, setText] = useState('');
    const [searchApi, results, errMessage] = useResults();
    
    const filterResultByPrice = (precio) =>
    {
        return results.filter( result => {
            return result.price === precio;
        });
    };
    return (
        <View>
            <SearchBar 
                term = {text} 
                onTermChange = {setText}
                onTermSubmited = {() => searchApi(text)}
            />
            {errMessage ? <Text>{errMessage}</Text>: null}
            <Text>Hemos obtenido {results.length} resultados</Text>
            <ResultList 
                title='Cost Effective!'
                results = {filterResultByPrice('$')}
            />
            <ResultList 
                title='Bit Pricier'
                results = {filterResultByPrice('$$')}
            />
            <ResultList 
                title='Big Spender'
                results = {filterResultByPrice('$$$')}
            />
        </View>
    );
};

const styles = StyleSheet.create({});

export default SearchScreen;