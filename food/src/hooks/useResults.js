import {useState, useEffect} from 'react';
import yelp from '../api/yelp';

const useResults = () =>
{
    const [results, setResults] =  useState([]);
    const [errMessage, setErrMessage] = useState('');
    const searchApi = async (text) =>
    {
        try
        {
            const response =  await yelp.get('/search', {
                params: {
                    limit: 50,
                    term: text,
                    location: 'san jose'
                }
            });
            setResults(response.data.businesses);
        }catch(err){
            setErrMessage('Algo salió mal :(');
        }
    };
    useEffect(() =>{
        searchApi('tacos');
    }, []);
    return [searchApi, results, errMessage];
};

export default useResults;