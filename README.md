# Sección 10: Manejando datos resultantes de la consulta a la API

Esta sección abarco todo el hecho del manejo de datos (el objeto `response`) que nos brindo la API al hacer la consulta e incluso se filtraron por su atributo `price` para poder renderizar nuestras tres listas principales de negocios de comida en nuestra `SearchScreen`.

El método solo consiste en desestructurar cada _item_ del objteto que nos regreso la API como respuesta, y para cada componente `ResultList` existente, obtener los que pertenezcan a su respectivo costo.

### Notas:
- Es muy facil el manejo de datos gracias a javascript, puesto que los objetos tipo _JSON_ son fáciles de manipular: sin embargo eso es lo que parece. Cada `ResultList` debe filtrar sus respectivos negocios en base a un paramtero dado, por lo que cada uno de estos componentes realiza una busqueda lineal en el objeto `response` que nos da la API. ¿Existirá una manera mas eficiente de hacer esto?

